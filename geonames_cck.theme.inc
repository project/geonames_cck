<?php

/**
 * @file
 * Theme functions for the geonames_cck module
 */


/**
 * Themes the disambiguation form
 *
 * @param unknown_type $form
 * @return unknown
 */
function theme_geonames_cck_disambiguate_form($form) {
  $name_title = $form['search']['name']['#title'];
  unset($form['search']['name']['#title']);
  $name = drupal_render($form['search']['name']);
  $country_title = $form['search']['country']['#title'];
  unset($form['search']['country']['#title']); 
  $country = drupal_render($form['search']['country']);
  $submit = drupal_render($form['search']['submit']);
  
  $output .= '<table><tr>';
  $output .= '<td>'.$name.'</td>';
  $output .= '<td>'.$country.'</td>';
  $output .= '<td>'.$submit.'</td>';
  $output .= '</tr></table>';
  
  $output .= drupal_render($form);
  return $output;
}

/**
 * Return custom text strings for given internal message IDs
 * 
 * We have chosen to not create an admin interface to manage these strings so that we do not break internationalisation. 
 * 
 * 
 * @param string $message
 * @param array $variables
 * @param string $country_code
 * @return t()
 */
function _geonames_cck_text($message, $variables = array(), $country_code = NULL) {
  switch ($message) {
    case 'disambiguation_multipleresults':
      $output .= t('There are @resultcount locations matching <strong>"!location"</strong>. Click on the best match to set your location or you can search again.', $variables);
      break;
    case 'disambiguation_noresults':
      $output .= '<p>'.t("The town <strong>\"!location\"</strong> wasn't found in the database. Please check your spelling and that the correct country is selected.", $variables).'</p>';
      if ($country_code == 'GB') {
        $output .= '<p>'.t('To search for a location in the UK, just give the city name without the county, e.g. "Oxford". For large cities, just search for the area name, e.g. "Chelsea".').'</p>';
      }
      elseif ($country_code == 'US') {
        $output .= '<p>'.t('To search for a location in the United States, just give the city name or the city name, state. E.g. search for "Dallas" or "Dallas, TX".').'</p>';
      }
      $output .= theme('geonames_cck_country_bounding_box', $country_code);
      break;
    case 'disambiguation_oneresult':
      $output .= t('There is 1 location matching <strong>"!location"</strong>. Click on the best match to set your location or you can search again.', $variables);
      break;
    case 'disambiguation_empty': // a location has never been set.
      $output .= t('You have not yet set your location. Use the form below to search for your location (name of city/town).', $variables);
      if ($country_code == NULL) { // for users that we don't even have a country for, assume GB
        $country_code = 'GB';
      }
      $output .= theme('geonames_cck_country_bounding_box', $country_code);
      break;
    case 'location_known':
      $output .= t('!location (<a href="@url">change</a>)', $variables);
      break;
    case 'location_nodata': // location is not yet set
      $output .= t('(<a href="@url">Set your location so we can show you who is teaching or learning your subjects in your area</a>)', $variables);
      break;
    case 'location_unknown': // unknown location entered on signup
      $output .= t('(<a href="@url">Set your location so we can show you who is teaching or learning your subjects in your area</a>)', $variables);
      break;
    case 'disambiguation_intro':
      $output .= t('School of Everything helps people who want to learn meet people who want to teach. By entering your location (name of city/town) you will receive better matches for the subjects you want to learn and teach.');
      break; 
  }
  return $output;
}


/**
 * Themes the display of an alert message to prompt user for better data  
 *
 * @param unknown_type $key
 */
function theme_geonames_cck_alert($message) {
  $output = '<div class="area area-interaction area-interaction-default">'.$message.'</div>';
  return $output;
}

/**
 * Theme the main disambiguation page.
 *  
 *
 * 
 */
function theme_geonames_cck_disambiguate_page($original_place, $original_country, $locations, $nid, $field, $index, $icon = NULL, $shadow = NULL) {
  if ($original_place) {
    $values = array(
        '!location' => $original_place .', '. geoapi_countries($original_country),
        '@resultcount' => $locations->total_results_count,
      );
    //How many matches do we have for the given search string?
    switch (sizeof($locations->results)) {
      case 0:  // no matches
        $message = _geonames_cck_text('disambiguation_noresults', $values, $original_country);
        break;
      case 1:  // only one match
        $message = _geonames_cck_text('disambiguation_oneresult', $values);
        $location_list = theme('geonames_cck_disambiguate_location', $original_place, $original_country, $locations, $nid, $field, $index);
        break;
      default:  // multiple matches
        $message = _geonames_cck_text('disambiguation_multipleresults', $values);  
        $location_list = theme('geonames_cck_disambiguate_location', $original_place, $original_country, $locations, $nid, $field, $index);
        break;
    }
  }
  else {
    // no place data, maybe no country either
    $message = _geonames_cck_text('disambiguation_empty', $values, $original_country);
  }
  $form = drupal_get_form('geonames_cck_disambiguate_form', $original_place, $original_country,  $nid, $field, $index);
  
  
  $output = '';
  $output .= '<div id="disambiguation">';
  $output .= '<div class="introduction">'. _geonames_cck_text('disambiguation_intro').'</div>';
  $output .= theme('geonames_cck_alert', $message . $form); 
  $output .= $location_list;
  $output .= '</div>';
  return $output;
}


/**
 * Produce disambiguation data for a specific location
 */
function theme_geonames_cck_disambiguate_location($original_place, $original_country, $locations, $nid, $field, $index) {
 
  foreach ($locations->results as $location) {
    $items[] = theme('geonames_cck_disambiguate_location_item', $location, $nid, $field, $index);
  }
  
  $title = t('Click on the best match to set your location', array('!location' => $original_place .', '. geoapi_countries($original_country)));
  $output = theme('item_list', $items, $title , 'ol', array('class' => 'disambiguation-list'));
  return $output;
}

/**
 * Theme one item of the disambiguation list
 */
function theme_geonames_cck_disambiguate_location_item($location, $nid, $field, $index) {
  $location_string = theme('geonames_cck_location_name', $location, true);
  $action_url = 'services/location/geonames/insert/'. $nid .'/'. $field .'/'. $index .'/'. $location['geonameid'];
  $output = l($location_string, $action_url, array(), null, null, false, true );
  $output .= theme('geonames_hidden', $location['lat'], $location['lng'], $counter .' - '. $location_string);
  return $output;
}

/**
 * Theme the full name for a location
 */
function theme_geonames_cck_location_name($location, $full = false) {
  $location_name = array();
  // fix capitalisation -- EVIL!
  if ($location['adminname2']) {
    $location['adminName2'] = $location['adminname2'];
    $location['adminName1'] = $location['adminname1'];
  }
  
  $location_name[] = $location['name'];
  if ($full or strtoupper($location['country']) == 'US') {
    if ($location['adminName2']) $location_name[] = $location['adminName2']; 
    if ($location['adminName1']) $location_name[] = $location['adminName1']; 
  }
  else if (strtoupper($location['country']) == 'GB') {
    if (!$location['adminName2']) {
      $location['adminName2'] = geonames_cck_get_admin_region(null , $location['lat'], $location['lng']);
    }
    if ($location['adminName2'] and $location['adminName2'] != $location['name']) {
      $location_name[] = $location['adminName2'];  // Fix the 'London, London' issue
    }
  }
  $result = implode(', ', $location_name);
  return $result;
}

/**
 * Used for default display setting
 * 
 * //TODO: get rid of extra params - just use attributes
 */
function theme_geonames_location($lat, $lng, $name='', $item=NULL, $node=NULL, $attributes = array(), $icon = NULL, $shadow = NULL) {
  $name and $location_name[] = $name;
  ($node) and $title = $node -> title;   // we have data, but no title
  if ($node) $attributes['nid'] = $node -> nid;
  if ($icon) {
    $attributes['gicon'] = $icon;
    $attributes['draggable'] = "false";
  }
  if ($shadow) $attributes['gshadow'] = $shadow;
  //drag&drop relocation disabled in this theming function
  $attributes['title'] = $title;
  
  if ($item) {
    // this is a cck item
    if (geoapi_countries($item['country'])) {
      $location_name[] = geoapi_countries($item['country']);
      if (is_array($location_name)) {
          $output = check_plain(implode(', ', $location_name));
      }
    }
  }
  else {
    if (is_array($location_name)) {
      $output = check_plain(implode(', ', $location_name));
    }
  }
  $output = theme('geonames_location_microformat', $lat, $lng, $output , $attributes ); 
  return $output;
}

/**
 * returns the basic html for a geo microformat
 *
 * @param unknown_type $lat
 * @param unknown_type $lng
 * @param unknown_type $text
 * @param unknown_type $attributes
 */
function theme_geonames_location_microformat($lat, $lng, $text = '' , $attributes = array()) {
  $attributes['class'] .= ' geo';
  if ($lat and $lng) {
    $output .= '<span class="latitude" title="'. $lat .'"></span>
                <span class="longitude" title="'. $lng .'"></span>';
    $output = '<div '.drupal_attributes($attributes).' >'. $output.$text .'</div>';
  }
  return $output;
}
/**
 * Used when 'With disambiguation link' option is selected
 * in fields' display setting. enables drag&drop of icons
 */
function theme_geonames_location_disambiguation($lat, $lng, $name='',  $field = NULL, $item=NULL, $node=NULL, $icon = NULL, $shadow = NULL) {
  $name and $location_name[] = $name;
  ($node) and $title = $node -> title;   // we have data, but no title
  if ($node) $attributes = 'nid="'. $node -> nid .'"';
  if ($icon) $attributes .= ' gicon = "'. $icon .'"';
  if ($shadow) $attributes .= ' gshadow = "'. $shadow .'"';
  
  //Icons are draggable only in node view, but not in disambiguation view
  //because it is meaningless to relocate multiple possible locations
  $draggable = '"false"'; //this is div's attribute, so it needs to be in double-quotations.
  
  if (arg(0) == 'node' && is_numeric(arg(1))) {
    //only allow users to relocate if they have edit access
    if (node_access('update', $node)) {
      $draggable = '"true"';
    }
  }
  //TODO: integrate with nodeprofile
  $attributes .= ' draggable = '. $draggable .' ';
  if ($field) {
      $attributes .= ' cckfield = '. $field['field_name'];
      $attributes .= ' cckfieldindex = 0';
  }    
            
  if ($item) {
    // this is a cck item
    geoapi_countries($item['country']) and $location_name[] = geoapi_countries($item['country']);
    $output = check_plain(implode(', ', $location_name));
    if (arg(0) == 'node' && is_numeric(arg(1))) {
      $output .= '<div class="disambiguate">'. l('(disambiguate)', arg(0) .'/'. arg(1) .'/disambiguate') .'</div>';
    }
  }
  else {
    $output = check_plain(implode(', ', $location_name));
  }
  if ($lat and $lng) {
  
    $output .= '<span class="latitude" title="'. $lat .'"></span>
                <span class="longitude" title="'. $lng .'"></span>';
    $output = '<div class="geo" title="'. $title .'" '. $attributes .' >'. $output .'</div>';
  }
  return $output;
}

/**
 * Used when <hidden> is selected in fields' display setting
 * Location information is invisible as content, but
 * locations get mapped
 */
function theme_geonames_hidden($lat, $lng, $title ='', $item=NULL, $node=NULL, $icon = NULL, $shadow = NULL) {
   if (!$title && $node) $title = $node -> title;
  if ($lat and $lng) $output = '<div class="geo" title="'. check_plain($title) .'" nid="'. $node -> nid .' ">
                   <span class="latitude" title="'. $lat .'"></span>
                   <span class="longitude" title="'. $lng .'"></span>
                   </div>';
  if ($icon) {
    $output .= '<span class="gicon" title="'. $icon .'">';
  }
  if ($shadow) {
   $output .= '<span class="gshadow" title="'. $shadow .'">';
  }
  return $output;
}

function theme_geonames_location_no_geotag($lat, $lng, $name='', $item=NULL, $node=NULL) {
  
  $name and $location_name[] = $name;
  
  ($node) and $title = $node -> title;   // we have data, but no title
  if ($node) $attributes = 'nid="'. $node -> nid .'"';
  //drag&drop relocation disabled in this theming function

  if ($item) {
    // this is a cck item
    geoapi_countries($item['country']) and $location_name[] = geoapi_countries($item['country']);
    $output = check_plain(implode(', ', $location_name));
  }
  else {
    $output = check_plain(implode(', ', $location_name));
  }
  
  return $output;
}


/**
 * Theme the display of the locations
 *@param node
 *   The node object
 *@param $disambiguate_links
 *   add links to disabmiguation pages
 *@param $attributes
 *   an array of attributes to be added to the rendered geodata
 * 
 * */
 //TODO: get title working
function theme_geonames_cck_location($node = null, $disambiguate_links = false, $attributes = array()) {
  // TODO: this should not be here. check that we can remove it
  if (!$node && arg(0) == 'node' && is_numeric(arg(1))) {
    $node = node_load(arg(1));
  }
  if ($node) {
    // make the geonames fields work 
    $geo_fields = geonames_cck_get_all_fields();
    // look for all the defined fields
    foreach ($geo_fields as $field_name => $geo_field_data) {
      if (is_array($node->$field_name)) {
        // this node uses this field
        foreach ($node->$field_name as $index => $field_data) {
          //get node-specific icon and shadow
          //(shadow is set only if icon is set)
          if ($icon = $field_data['icon']) {
            $shadow = $field_data['shadow'];
          }
         
          //if node-specific icon and shadow are not set, get them from field setting
          if (!$icon) {
            if ($icon = $geo_field_data['icon']) {
              $shadow = $geo_field_data['shadow'];
            }
          }
          $locations['geonames'][] = theme('geonames_cck_location_item', $field_data, $node, $disambiguate_links, $icon, $shadow, $attributes);
        }
      }
    }
    if (module_exists('token')) {
      token_replace($title, 'node', $node);
    }
    if (is_array($locations['geonames'])) {
      $output = $title .'<div class="locations-geonames">'. implode(', ', $locations['geonames']) .'</div>';
    }
  }
  if ($output) {
    return '<div class="geonames-locations">'. $output .'</div>';
  }
}

/**
 * Theme a geonames entry in the locations list.
 */
function theme_geonames_cck_location_item($location, $node, $disambiguate_links = false, $icon = NULL, $shadow = NULL, $attributes = array() ) {
  global $user;
  if ($location['name'] && $location['lat']) {
    // we have location data
    $values = array(
      '!location' => theme('geonames_location', $location['lat'], $location['lng'], theme('geonames_cck_location_name', $location), $location, $node, $attributes, $icon, $shadow),
      '@country' => geoapi_countries($location['country']),
      '@url' => url('node/'. $node->nid .'/disambiguate'),
    );
    if ( $disambiguate_links && node_access ('update', $node)) {
      $output = _geonames_cck_text('location_known', $values);
    }
    else {
      $output = theme('geonames_location', $location['lat'], $location['lng'], theme('geonames_cck_location_name', $location), $location, $node, $attributes, $icon, $shadow );
    }
  }
  else if ($location['name'] && !$location['lat']) {
    // we have a name, but can't work out where it is
    $values = array(
      '!location' => $location['name'],
      '@country' => geoapi_countries($location['country']),
      '@url' => url('node/'. $node->nid .'/disambiguate'),
    );
    if ($disambiguate_links && node_access('update', $node)) {
      $output = _geonames_cck_text('location_unknown', $values);
    }
    else {
      $output = geoapi_countries($location['country']);
    }
    // add a bounding box map
    $output .= theme('geonames_cck_country_bounding_box', $location['country']);
  }
  else {
    //no location data has been provided
    if ($disambiguate_links && node_access('update', $node)) {
      $values = array(
        '@country' => geoapi_countries($location['country']),
        '@url' => url('node/'. $node->nid .'/disambiguate'),
      );
      $output = _geonames_cck_text('location_nodata', $values);
    }
    else {
      $output = geoapi_countries($location['country']);
    }
    $output .= theme('geonames_cck_country_bounding_box', $location['country']);
  }
  //print $values['test'];
  return $output;
}



/**
 * Generates a set of four hidden points that will force geomap to zoom to a country
 *
 * @param unknown_type $country
 */
function theme_geonames_cck_country_bounding_box($country) {
  if (module_exists('geonames_countryinfo')) {
    $data = _geoapi_get_country_data($country);
    $output .= theme('geonames_location_microformat', $data['bboxnorth'], $data['bboxwest'], '' , array('gicon' => 'null', 'gshadow' => 'null') );
    $output .= theme('geonames_location_microformat', $data['bboxsouth'], $data['bboxeast'], '' , array('gicon' => 'null', 'gshadow' => 'null') );
  }
  return $output;
}


function theme_geoapi_kml_item_simple($geonameid, $name = null , $description = null){
  $geodata = _geoapi_get_data_for_geonameid($geonameid);
  if (!$name) {
    $name = $geodata['name'];
  }
  $output .= '<name>'.$name.'</name>'."\n";
  if ($description) {
    $output .= '<description>'.$description.'</description>'."\n";
  }
  $output.= '<Point><coordinates>'.$geodata['lat'].','.$geodata['lng'].'</coordinates></Point>'."\n";
  $output = '<Placemark>'.$output.'</Placemark>';
  return $output; 
}

function theme_geoapi_kml_document($content) {
  $output = '<?xml version="1.0" encoding="UTF-8"?>'."\n";
  $output .= '<kml xmlns="http://www.opengis.net/kml/2.2">';
  $output .= $content;
  $output .= '</kml>';
  return $output;
}

/**
 * themes a description of what geonames is
 *
 * @return string
 */
function theme_geonames_description() {
  $output = theme('everything_help', array('context' => array('object' => 'person', 'perspective' => 'disambiguate'))); // set context here
  return $output;
}