<?php

/**
 * @file
 * The geonames_cck module implements basic geolocation tools using geonames web services.
 * 
 * This module was designed to work with the geomap module. Locations will be output using 
 * Geo microformats, and the geomap module will display any geo data on a google map. 
 * 
 * TODO: Make multiple values work. Some work has been done, but, the form does not currently handle multiples
 * TODO: Make places work. A stab has been made at "things nearby" but it's not really production level.
 * TODO: Views integration. Since it's cck, there are come views tools, but not enough to really use.  
 * 
 */

/**
 * implementation of hook_perm
 *
 * @return array
 */
function geonames_cck_perm() {
  return array('choose custom icon for node');
}

/**
 * implementation of hook_menu()
 * @param boolean $may_cache
 * @return array
 */
function geonames_cck_menu($may_cache) {
  if ($may_cache) {
    $items[] = array(
      'path' => 'admin/settings/geonamescck/main',
      'title' => t('Geonames CCK'),
      'description' => t('Settings for Geonames cck module'),
      'type' => MENU_DEFAULT_LOCAL_TASK, // optional
    );
  }
  else {
    /**
     * Places are not ready yet.
     * 
    $items[] = array(
      'path' => 'place',
      'title' => t('places'),
      'description' => t('Display a place'),
      'callback' => 'geonames_cck_place_page',
      'access' => user_access('access place pages'), 
      'type' => MENU_CALLBACK,
    );
    */
    if (arg(0) == 'node' && is_numeric(arg(1))) {
      $items[] = array(
        'path' => 'node/'. arg(1) .'/disambiguate',
        'title' => t('Where are you based?'),
        'access' => node_access('update', node_load(arg(1))),
        'callback' => 'geonames_cck_disambiguate',
        'callback arguments' => array(arg(1)),
        'type' => MENU_CALLBACK, // This may work as a local task.
      );
    }
  }
  return $items;
}

/**
 * implementation of hook_block() 
 *
 */
function geonames_cck_block($op = 'list', $delta = 0, $edit = array()) {
  switch ($op) {
    case 'list':
      $blocks = array(
        'locations' => array('info' => t('geonames node locations list')),
        'geonames_description' => array('info' => t('geonames - description of geonames')),
      );
      return $blocks;
      break;
    case 'view':
      if (arg(0) == 'node' && is_numeric(arg(1)) && !arg(2)) {
        $node = node_load(arg(1));
       
        switch ($delta) {
          case 'locations':
            $block['content'] = theme('geonames_cck_location', $node, true);
            break;
        }
      }
      if (arg(0) == 'node' && is_numeric(arg(1)) && arg(2) == 'disambiguate') {
        switch ($delta) {
          case 'geonames_description':
            $block['content'] = theme('geonames_description');
            break;
        }
      }
      return $block;
    break;
  }
}



/**
 * Implementation of hook_field_info().
 */
function geonames_cck_field_info() {
  return array(
    'geonames_cck' => array('label' => 'Geonames location'),
  );
}

/**
 * Implementation of hook_field_settings().
 */
function geonames_cck_field_settings($op, $field) {
  switch ($op) {
    case 'form':
      $form = array();
      return $form;

    case 'validate':
      break;

    case 'save':
      return array('name', 'geonameId', 'country', 'lat', 'lng', 'adminName1', 'adminCode1', 'adminName2', 'adminCode2', 'fcode', 'icon', 'shadow');

    case 'database columns':
      $columns = array(
        'name' => array('type' => 'varchar', 'length' => 250, 'not null' => TRUE),
        'geonameId' => array('type' => 'varchar', 'length' => 20, 'not null' => TRUE),
        'country' => array('type' => 'varchar', 'length' => 2, 'not null' => TRUE),
        'lat' => array('type' => 'float', 'length' => 10, 'not null' => FALSE),
        'lng' => array('type' => 'float', 'length' => 10, 'not null' => FALSE),
        'adminName1' => array('type' => 'varchar', 'length' => 100, 'not null' => TRUE),
        'adminCode1' => array('type' => 'varchar', 'length' => 10, 'not null' => TRUE),
        'adminName2' => array('type' => 'varchar', 'length' => 100, 'not null' => TRUE),
        'adminCode2' => array('type' => 'varchar', 'length' => 10, 'not null' => TRUE),
        'fcode' => array('type' => 'varchar', 'length' => 10, 'not null' => FALSE),
        'icon' => array('type' => 'varchar', 'length' => 250, 'not null' => FALSE),
        'shadow' => array('type' => 'varchar', 'length' => 250, 'not null' => FALSE),
      );
      return $columns;
    case 'filters':
      // Filters are managed through the views include file
  }
}

/**
 * Implementation of hook_field().
 */
function geonames_cck_field($op, &$node, $field, &$items, $teaser, $page) {
  switch ($op) {
    case 'load':
      break;
    case 'view':
      break;
    case 'insert':
      break;
    case 'update':
      break;
    case 'delete':
      break;
  }
  return $output;
}

/**
 * Implementation of hook_widget_info().
 */
function geonames_cck_widget_info() {
  return array(
    'geoname_cck' => array(
      'label' => t('Global Location'),
      'field types' => array('geonames_cck'),
    ),
  );
}


/**
 * Implementation of hook_widget_settings().
 */
function geonames_cck_widget_settings($op, $widget) {
  
  switch ($op) {
    case 'form':
      $form = array();
      $form['icon'] = array(
        '#type' => 'textfield',
        '#title' => t('Custom icon'),
        '#default_value' => $widget['icon'] ? $widget['icon'] : '',
        '#required' => FALSE,
        '#description' => t('To set a default icon for this field, add a full URL of the icon. '. l('List of icons', 'http://www.visual-case.it/cgi-bin/vc/GMapsIcons.pl', array('rel' => 'nofollow', 'target' => 'blank'))),
      );
      $form['shadow'] = array(
        '#type' => 'textfield',
        '#title' => t('Shadow for custom icon'),
        '#default_value' => $widget['shadow'] ? $widget['shadow'] : '',
        '#required' => FALSE,
        '#description' => t('To set a default icon&apos;s shadow for this field, add a full URL of the shadow'),
      );
      return $form;
      
    case 'validate':
    $field_names = array('icon', 'shadow');
    foreach ($field_names as $fname) {
      if ($widget[$fname]) {
        if (!valid_url($widget[$fname], true)) {
         form_set_error($fname, 'invalid URL');
        }
      }
    }

      break;
      
    case 'save':
      return array('icon', 'shadow');
  }
}

/**
 * Implementation of hook_widget().
 */
function geonames_cck_widget($op, &$node, $field, &$items) {
  $fieldname = $field['field_name'];
  switch ($op) {
    case 'prepare form values':
      break;

    case 'form':
      $form = _geonames_cck_widget_form($node, $field, $items);
      return $form;
      
    case 'validate':
      foreach ($items as $delta => $item) {
        if (!$item['geonameId'] || ($item['name'] != $item['old_name'] OR $item['country'] != $item['old_country'])) {
          // location has changed
          $search_results = geoapi_convert_location_to_geo($item['name'], $item['country']);
          if ( !$search_results -> results ) {
            // We are not using for_set_error here because we use the disambiguation pages to fix things
            $values = array('@name' => $item['name']);
            
            $message = t('Your location "@name" was not found in our locations database.');
            if ($node->nid) {
              $values['!link'] = l( t('Find another location?'), 'node/'. $node->nid .'/disambiguate');
              $message .= ' !link';
            }
            drupal_set_message(t( $message , $values ));
          }
        }
      }
      //icon url
       
      break; 
    case 'process form values':
      foreach ($items as $delta => $item) {        
        if ($item['name'] != $item['old_name'] OR $item['country'] != $item['old_country']) {
          // location has changed
          // This code currently duplicates the validation code. Not perfect, but results are cached.
          // TODO: Clean up the validation process
          $search_results = geoapi_convert_location_to_geo($item['name'], $item['country']);
          $locations = $search_results -> results;
          if ($search_results -> total_results_count) {
            $items[$delta] = _geoapi_convert_geonames_result_to_cck_value($locations[0]);
          }
          else {
            // clear any old values
            unset($items[$delta]['lat']);
            unset($items[$delta]['lng']);
          }
        }
      }
      break;
  }
}


/**
 * Generate the form for the widget
 *
 */
function _geonames_cck_widget_form($node, $field, $items) {
  if ($field['required']) {
    // make field not required on admin pages
    $required = (!(arg(0) == 'admin' && arg(1) == 'content')) ? true : false;
  }
  $fieldlabel = $field['widget']['label'];
  $fieldname = $field['field_name'];
  $fieldname_css = 'edit-'. strtr($field['field_name'], '_', '-');
  $form = array();
  $form[$fieldname]['#tree'] = true;
  $count = 0;
  $form[$fieldname][$count]=array(
    'name' => array(
      '#type' => 'textfield',
      '#description' => t($field['widget']['description']),
      //'#title' => t('Location'),
      '#title' => $fieldlabel,
      '#size' => 30,
      '#maxlength' => 250, 
      '#default_value' => $items[$count]['name'] ? $items[$count]['name'] : geoapi_get_default_city(),
      '#attributes' => array('class' => 'float'),
    ),    
    'country' => array(
      '#type' => 'select',
      '#title' => $fieldlabel .'&nbsp;'. t('country'),
      '#default_value' => $items[$count]['country'] ? strtoupper($items[$count]['country']) : geoapi_get_default_country(),
      '#attributes' => array('class' => 'float'),
      '#options' => geoapi_countries(),
      '#multiple' => false,
      '#required' => $required, 
    ),
    'lat' => array(
      '#type' => 'hidden',        
      '#default_value' => $items[$count]['lat'],
    ),
    'lng' => array(
      '#type' => 'hidden',
      '#default_value' => $items[$count]['lng'],
    ),
    'old_name' => array(
      '#type' => 'value',
      '#value' => $items[$count]['name']
    ),
    'old_country' => array(
      '#type' => 'value',
      '#value' => $items[$count]['country']
    ),
    'icon' => array(
      '#type' => 'textfield',
      '#description' => t('If you want to set a custom icon for this location, enter the full URL of the icon here.'. l('List of icons', 'http://www.visual-case.it/cgi-bin/vc/GMapsIcons.pl', array('rel' => 'nofollow', 'target' => 'blank'))),
      '#title' => $fieldlabel .'&nbsp;'. t('Icon'),
      '#size' => 30,
      '#maxlength' => 250, 
      '#default_value' => $items[$count]['icon'] ? check_url($items[$count]['icon']) : '',
      //'#attributes' => array('class' => 'float'),
    ),
    'shadow' => array(
      '#type' => 'textfield',
      '#description' => t('If you want to set a custom icon shadow for this location, enter the of the icon here.'),
      '#title' => $fieldlabel .'&nbsp;'. t('Shadow'),
      '#size' => 30,
      '#maxlength' => 250,
      '#default_value' => $items[$count]['shadow'] ? check_url($items[$count]['shadow']) : '',
      //'#attributes' => array('class' => 'float'),
    ),
    'geonameId' => array(
      '#type' => 'value',
      '#value' => $items[$count]['geonameId']
    ),
  );
  return $form;
}


/**
 * Implementation of hook_field_formatter_info().
 */
function geonames_cck_field_formatter_info() {
  $formatters = array(
    'default' => array(
      'label' => t('Default'),
      'field types' => array('geonames_cck'),
    ),
    'no_geotag' => array(
      'label' => t('No geotag'),
      'field types' => array('geonames_cck'),
    ),
    'geo_hidden' => array(
      'label' => t('Hidden geo code'),
      'field types' => array('geonames_cck'),
    ),
    'disambiguation' => array(
      'label' => t('With disambiguation link'),
      'field types' => array('geonames_cck'),
    ),


  );
  return $formatters;
}

/**
 * Implementation of hook_field_formatter().
 */
function geonames_cck_field_formatter($field, $item, $formatter, $node) {
  if (!isset($item['name'])) {
    return '';
  }
  $field_name = array('icon', 'shadow');
  $n = count($field_name);
  
  //get node-specific icon if available
  for ($i=0; $i<$n; $i++) {
        
    if ($item[$field_name[$i]]) {
    	
      ${$field_name[$i]} = check_url($item[$field_name[$i]]);
    }
    //get widget's default icon if available
    else if ($field[$field_name[$i]]) {
      
     ${$field_name[$i]} = check_url($field[$field_name[$i]]);
    }
    else{
      
      ${$field_name[$i]}='';
    }
  } 

    switch ($formatter) {
    case 'geo_hidden':
      $output = theme('geonames_hidden', $item['lat'], $item['lng'], theme('geonames_cck_location_name', $item), $item,  $node, $icon, $shadow);
      break;
    case 'disambiguation':
      $output = theme('geonames_location_disambiguation', $item['lat'], $item['lng'], theme('geonames_cck_location_name', $item), $field, $item, $node, $icon, $shadow);      
      break;
    case 'no_geotag':
      $output = theme('geonames_location_no_geotag', $item['lat'], $item['lng'], theme('geonames_cck_location_name', $item), $item, $node);
      break;
    default:
      $output = theme('geonames_location', $item['lat'], $item['lng'], theme('geonames_cck_location_name', $item), $item, $node, array(), $icon, $shadow);

  }
  return $output;
}


// =========================== Disambiguation

/**
 * Manage the disambiguation for a node. This allows a user to say that the location we selected was incorrect. 
 * This is added to menu as a sub-path for each node. 
 * We look at various parameters to find out which fields to disambigute
 * 
 * @param integer $nid
 */

function geonames_cck_disambiguate($nid) {
  // first we need to check what field to work with
  $node = node_load($nid);
  if ($_REQUEST['field']) {
    $fields[$_REQUEST['field']] = $_REQUEST['field'];
  }
  else {
    //TODO: Multiple field disambiguation would require some presentation work. (it's also untested)
    // no field was specified, so lets do them all
    // we might have more than one geonames field. We need to handle all of them
    $type_info = content_types($node->type);
    $all_fields = $type_info['fields'];
    if (is_array($all_fields)) {
      foreach ($all_fields as $field) {
        ($field['type'] == 'geonames_cck') and $fields[$field['field_name']] = $field['field_name'];
      }
    }
    //$fields = geonames_cck_get_all_fields()
  }
  // get place and country info from node
  foreach ($fields as $field) {    
    // if we have a field index, and a new name and country,
    // then we work only with that field value.
    // otherwise we have to work with all values
    
    if ($_REQUEST['field'] && !is_null($_REQUEST['index']) && $_REQUEST['name'] && $_REQUEST['country']) {
      $indexes = array($_REQUEST['index']); // set the index
    } 
    else {
      // set up indexes array to process all field values
      //for ($count = 0; $count < sizeof($field); $count++) {
        $indexes[] = 0;
      //}
    }
    
    foreach ($indexes as $index) {
      // work out which location we need to disambiguate
      if ($_REQUEST['name']) {
        $place = check_plain($_REQUEST['name']);
      }
      else {
        $place = $node -> {$field}[$index]['name'];
      }
      if ($_REQUEST['country']) {
        $country = check_plain($_REQUEST['country']);
      }
      else {
        $country = $node -> {$field}[$index]['country'];
      }
      if ($place) {
        $locations = geoapi_convert_location_to_geo($place, $country);
      }
      $output .= theme('geonames_cck_disambiguate_page', $place, $country, $locations, $nid, $field, $index, $icon, $shadow);
    }
  }
  return $output;
} 

/**
 * Determine which admin reqion we want to allocate this location to.
 * Sometimes we want to create boundarys for locations to make things
 * more intuitive for our users. (London in particular) 
 * TODO: Create an admin interface for this
 */
function geonames_cck_get_admin_region($adminname, $lat, $lng) {
  $regions = array(
    'London' => array(
      'topleft' => array('lat' => 51.655 , 'lng' => 0.186), 
      'bottomright' => array('lat' => 51.352 , 'lng' => -0.453 )
    )
  );
  foreach ($regions as $name => $region) {
    if ($lat < $region['topleft']['lat'] && $lat>$region['bottomright']['lat']
        && $lng > $region['bottomright']['lng'] && $lng < $region['topleft']['lng']) {
      $adminname = $name;
    }
  }
  return $adminname;
}

/**
 * Define a form for the disambiguation page. 
 * Allows a user to specify location
 */
function geonames_cck_disambiguate_form($city, $country, $nid, $field, $index) {
  $form['nid'] = array(
   '#type' => 'value',
   '#value' => $nid,
  );
  $form['field'] = array(
   '#type' => 'value',
   '#value' => $field,
  );
  
  $form['index'] = array(
   '#type' => 'value',
   '#value' => $index,
  );
  
  $form['search'] = array(
    '#description' => '',
  );
  $form['search']['name'] = array(
    '#type' => 'textfield',
    '#title' => t('Name of city or town'),
    '#size' => 20,
    '#maxlength' => 250, 
    '#default_value' => $city,
    '#attributes' => array('class' => 'float'),
  );
  $form['search']['country'] = array(
    '#type' => 'select',
    '#title' => t('Country'),
    '#default_value' => strtoupper($country),
    '#attributes' => array('class' => 'float'),
    '#options' => geoapi_countries_trim(30),
    '#multiple' => false,
  );
  $form['search']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Find my town'),  // Note: translating button labels can be troublesome in Drupal 5. Everything works fine at the moment but developers extending this module should bear this in mind.
  );
  $form['#action'] = url('node/'.$nid.'/disambiguate');
  // We use the get method here to make this form just send it's data back to the page
  $form['#method'] = 'get';
  return $form;
}

// =============helpers ========================//


/**
 * Get the names of all geonames_cck fields. 
 *
 * @return $array A list of field objects
 */
function geonames_cck_get_all_fields() {
  $fields = content_fields();
  if (is_array($fields)) {
    foreach ($fields as $field) {
      ($field['type'] == 'geonames_cck') and $results[$field['field_name']] = $field;
    }
  }
  return $results;
}

/**
 * Get the names of all geonames_cck_fields in option format.
 * The output of this function can be used to populate select lists
 * 
 */
function geonames_cck_get_all_fields_options() {
  $fields = geonames_cck_get_all_fields();
  $options = array();
  if (is_array($fields)) {
    foreach ($fields as $field) {
      $options[$field['field_name']] = $field['field_name'];
    }
  }
  return $options;
}

/**
 * retuns an array of locations for a node
 *
 * @param unknown_type $node
 * @return array List of locations
 */
function geonames_cck_get_all_node_locations($node) {
  static $locations = array();
  if (!$locations[$node->nid]) {
    $locations[$node->nid] = array();
    $geo_fields = geonames_cck_get_all_fields();
    //look for all the defined fields
    foreach ($geo_fields as $field_name => $field_data) {
      if (is_array($node->$field_name)) {
        // this node uses this field
        foreach ($node->$field_name as $index => $field_data) {
          $locations[$node->nid][]= $field_data;
        }
      }
    }
  }
  return $locations[$node->nid];
}



/**
 * Disables multiple field option (temporary)
 */
function geonames_cck_form_alter($form_id, &$form) {
  if (!user_access('choose custom icon for node')) {
    //remove icon option if the user has no permission
    unset($form['field_geonames_location'][0]['icon']);
    unset($form['field_geonames_location'][0]['shadow']);
  }
  if (arg(0)=='admin' && arg(1)=='content' && arg(2)=='types' && $form['field_type']['#value']=='geonames_cck') {
    //unset($form['field']['multiple']);
      $form['field']['multiple']['#disabled'] = true;
      $form['field']['multiple']['#description']=t('Sorry, this option is not yet available for geonames CCK');
  }
}


// ===================  includes ================= //

include_once('geonames_cck.theme.inc');
if (module_exists('views')) {
  require_once(drupal_get_path('module', 'geonames_cck') .'/geonames_cck.views.inc');
}
