<?php

define ('DEBUG', FALSE);

function geonames_cck_views_tables() {
  //get a list of geonames cck fields
  $geo_cck = geonames_cck_get_all_fields();
  $geo_tables = array();  
  //get the table info for each field
  foreach ($geo_cck as $geo_field) {
    $geo_cck[$geo_field['field_name']]['table_info'] = content_database_info($geo_field);
  }
  
  //create the table array for Views
  foreach ($geo_cck as $geo_field) {
    //print $geo_field['table_info']['table']."<br/>\n";
    $table = array(
      'name' => $geo_field['table_info']['table'],
      'join' => array(
        'left' => array(
          'table' => 'node',
          'field' => 'nid'
        ), 
        'right' => array(
          'field' => 'nid'
        ),
      ),
      'fields' => array(
        //location name field
        $geo_field['table_info']['columns']['name']['column'] => array(
          'name' => t('Geonames CCK: Location name (@field)', array('@field' => $geo_field['field_name'])),
          'sortable' => TRUE,
        ),
        //country field
        $geo_field['table_info']['columns']['country']['column'] => array(
          'name' => t('Geonames CCK: Country (@field)', array('@field' => $geo_field['field_name'])),
          'sortable' => TRUE,        
        ),
      ),
      'filters' => array(
        //filter for location name (freetext)
        $geo_field['table_info']['columns']['name']['column'] => array(
          'name' => t('Geonames CCK: Location name for @s', array('@s' => $geo_field['field_name'])),
          'help' => 'Enter a name of city you want to search',
          //by omitting 'value', a textfield is automatically added by Views
          //'value'=> array (geonames_cck_views_textfield()),
          'operator' => 'views_handler_operator_like',
          'handler' => 'views_handler_filter_like'
        ),
        //filter for country (select)
        $geo_field['table_info']['columns']['country']['column'] => array(
          'name' => t('Geonames CCK: country for @s', array('@s' => $geo_field['field_name'])),
          'help' => t("This allows you to filter nodes with geonames CCK field @s by country", array('@s' => $geo_field['field_name'])),
          'list' => array(geoapi_countries()),
          'list-type' => 'select',
          'operator' => 'views_handler_operator_eqneq', 
        ),
      ),
    );

    $tables[$geo_field['table_info']['table']] = $table;
  }
  return $tables;
  
}


function geonames_cck_views_arguments() {
  $arguments = array( 
    'country' => array(
      'name' => t('geonames cck: Country'),
      'handler' => 'geonames_cck_handler_country',
      'help' => t('Limit nodes to a specific country. Select which field to use in the options'),    
      'option' => array('#type' => 'select', '#options' => geonames_cck_get_all_fields_options(), '#multiple' => false ),    
    ),      
    'proximity'  => array(
      'name' => t('geonames cck: proximity'),
      'handler' => 'geonames_cck_handler_proximity',
      'help' => t('Show nodes near a geoname id. '), 
      'option' => array('#type' => 'select', '#options' => geonames_cck_get_all_fields_options(), '#multiple' => false ),       
    ),
    'node_proximity'  => array(
      'name' => t('geonames cck: proximity to node'),
      'handler' => 'geonames_cck_handler_proximity_node',
      'help' => t('Show nodes near a node. '), 
      'option' => array('#type' => 'select', '#options' => geonames_cck_get_all_fields_options(), '#multiple' => false ),       
    ),
    'bounding_box'  => array(
      'name' => t('geonames cck: bounding box'),
      'handler' => 'geonames_cck_handler_bounding box',
      'help' => t('Show nodes near a geoname id. '), 
      'option' => array('#type' => 'select', '#options' => geonames_cck_get_all_fields_options(), '#multiple' => false ),       
    ),
    'bounding_box_node'  => array(
      'name' => t('geonames cck: bounding box, centered on node'),
      'handler' => 'geonames_cck_handler_bounding_box_node',
      'help' => t('Show nodes near a node. '), 
      'option' => array('#type' => 'select', '#options' => geonames_cck_get_all_fields_options(), '#multiple' => false ),       
    ),    
  );
  if (module_exists('nodeprofile')) {
    // lets allow some nodeprofile extensions
    
  }
  return $arguments;
}


function geonames_cck_handler_country($op, &$query, $a1, $a2 = '') {

switch ($op) {
    case 'summary':
      
      
      break;
    case 'filter':            
      $field = content_fields($a1['options']);
      $db_info = content_database_info($field);
      $table = $db_info['table'];      
      $country = $a2;
      $country_field = $db_info['columns']['country']['column'];
      
      $joininfo = array( 'right' => array( 'table' => $table, 'field' => "nid" ),
                         'left' => array( 'table' => 'node', 'field' => 'nid'),
                         'type' => 'inner',                      
                      );                  
      $query->add_where( $table .'.'. $country_field ." = '%s'" , $country);
      $query->add_table($table, false, 1, $joininfo );
      
      break;
    case 'link':
      //$node = node_load( $query->field_related_content_nid );
      //return l( $node->title, $a2. '/'. intval( $query->field_related_content_nid )  );
      break;
    case 'title':
      //TODO: This is not working. I am not getting any titles out
      if ( $query ) {
        //$node=node_load( $query );
        //return $node->title;
      }
      break;
  }
}

function geonames_cck_handler_proximity($op, &$query, $a1, $a2 = '') { 
  switch ($op) {
    case 'summary':
      
      
      break;
    case 'filter':
      $geonameid = geoapi_get_geonameid_for_alias($a2);
      $geodata = _geoapi_get_data_for_geonameid($geonameid);
      $field = content_fields($a1['options']);
      geonames_cck_proximity_query(&$query, $field, $geodata);
      break;
    case 'link':
      break;
    case 'title': 
      if ( $query ) {
        $geodata = _geoapi_get_data_for_geonameid(geoapi_get_geonameid_for_alias($query));
        return $geodata['name'];
      }
      break;
  }
}


function geonames_cck_handler_proximity_node($op, &$query, $a1, $a2 = '') {
switch ($op) {
    case 'summary':
      
      
      break;
    case 'filter':
      if ($a2) {
        $field = content_fields($a1['options']);
        $nid= $a2;
        $node = node_load($nid);        
        $geodata = $node->{$field['field_name']}[0];
        if ($geodata['lat'] && $geodata['lng']) {
          geonames_cck_proximity_query(&$query, $field, $geodata);
        }
      }
      break;
    case 'link':
      //$node = node_load( $query->field_related_content_nid );
      //return l( $node->title, $a2. '/'. intval( $query->field_related_content_nid )  );
      break;
    case 'title':
      if ( $query ) {
        $node = node_load($query);
        return $node->title;
      }
      break;
  }
}


function geonames_cck_handler_bounding_box($op, &$query, $a1, $a2 = '') { 
  switch ($op) {
    case 'summary':
      
      
      break;
    case 'filter':
      if ($a2) {
        $geonameid = $a2;
        $geodata = _geoapi_get_data_for_geonameid($geonameid);
        $field = content_fields($a1['options']);
        geonames_cck_bounding_box_query(&$query, $field, $geodata);
      }
      break;
    case 'link':
      break;
    case 'title': 
      if ( $query ) {
        $geodata = _geoapi_get_data_for_geonameid($query);
        return $geodata['name'];
      }
      break;
  }
}
/**
 * Views handler to limit nodes to bounding box lat and lng
 *
 * @param unknown_type $op
 * @param unknown_type $query
 * @param unknown_type $a1
 * @param unknown_type $a2
 * @return unknown
 */
function geonames_cck_handler_bounding_box_node($op, &$query, $a1, $a2 = '') {
switch ($op) {
    case 'summary':
      
      
      break;
    case 'filter':
      if ($a2) {
        $field = content_fields($a1['options']);
        $nid= $a2;
        $node = node_load($nid);        
        $geodata = $node->{$field['field_name']}[0];
        if ($geodata['lat'] && $geodata['lng']) {
          geonames_cck_bounding_box_query(&$query, $field, $geodata);
        }
      }
      break;
    case 'link':
      //$node = node_load( $query->field_related_content_nid );
      //return l( $node->title, $a2. '/'. intval( $query->field_related_content_nid )  );
      break;
    case 'title':
      if ( $query ) {
        $node = node_load($query);
        return $node->title;
      }
      break;
  }
}


/**
 * Alters a views query object with proximity sort data 
 *
 * @param unknown_type $query
 * @param unknown_type $field
 * @param unknown_type $geodata
 */
function geonames_cck_proximity_query(&$query, $field, $geodata, $left_table = 'node', $distance = 50) {
  include_once(drupal_get_path('module', 'geonames_cck') .'/geonames_cck_earth.inc');            
  $db_info = content_database_info($field);
  $table = $db_info['table'];
  $latcol = $db_info['columns']['lat']['column'];
  $lngcol = $db_info['columns']['lng']['column'];
  $lat = $geodata['lat'];
  $lon = $geodata['lng'];
  if (!$lat || !$lon) return; 
  $divisor = 1609.347;
  $latrange = earth_latitude_range($lon, $lat, ($distance * $divisor));
  $lonrange = earth_longitude_range($lon, $lat, ($distance * $divisor));
  $joininfo = array( 'right' => array( 'table' => $table, 'field' => "nid" ),
                         'left' => array( 'table' => $left_table, 'field' => 'nid'),
                         'type' => 'inner',                      
                      );                  
  $query->add_table($table, false, 1, $joininfo );
  $query->add_orderby(NULL, earth_distancesqlared_sql($lon, $lat, $table, $latcol, $lngcol), 'ASC', 'distanceSquared');
  $query->add_where("$table.$lngcol IS NOT NULL");
  $query->add_where("$table.$latcol > %f AND $table.$latcol < %f AND $table.$lngcol > %f AND $table.$lngcol < %f", $latrange[0], $latrange[1], $lonrange[0], $lonrange[1]);
}

/**
 * Alters a views query object with proximity sort data 
 *
 * @param unknown_type $query
 * @param unknown_type $field
 * @param unknown_type $geodata
 */
function geonames_cck_bounding_box_query(&$query, $field, $geodata, $left_table = 'node') {
  include_once(drupal_get_path('module', 'geonames_cck') .'/geonames_cck_earth.inc');            
  $db_info = content_database_info($field);
  $table = $db_info['table'];
  $latcol = $db_info['columns']['lat']['column'];
  $lngcol = $db_info['columns']['lng']['column'];
  $lat = $geodata['lat'];
  $lon = $geodata['lng'];
  if (!$lat || !$lon) return;
  $distance = 50; 
  $divisor = 1609.347;
  $latrange = earth_latitude_range($lon, $lat, ($distance * $divisor));
  $lonrange = earth_longitude_range($lon, $lat, ($distance * $divisor));
  $joininfo = array( 'right' => array( 'table' => $table, 'field' => "nid" ),
                         'left' => array( 'table' => $left_table, 'field' => 'nid'),
                         'type' => 'inner',                      
                      );                  
  $query->add_table($table, false, 1, $joininfo );
  $query->add_where("$table.$lngcol IS NOT NULL");
  $query->add_where("$table.$latcol > %f AND $table.$latcol < %f AND $table.$lngcol > %f AND $table.$lngcol < %f", $latrange[0], $latrange[1], $lonrange[0], $lonrange[1]);
}



function geonames_cck_views_textfield() {
  return array(
   $form['value'] = array(
    '#type' => 'textfield',
    '#title' => t('Location name'),
    '#default_value' => '',
    '#size' => 40,
    '#maxlength' => 40,
    '#description' => t('Enter the name of location'),
    )
  );
  
}

/**
 * returns all geonames_cck fields with detailed information
 */
function geonames_cck_get_all_fields2() {
  //1. get a list of content types
  //2. create a list of geonames cck field

  $field_list = content_types();
  $geo_cck = array();
  $geo_cck_fieldname = array();
  foreach ($field_list as $content_types => $content_info) {
    foreach ($content_info['fields'] as $field_info => $field_detail) {
      //keep the field info if not found previously
      if (!in_array($field_detail['field_name'], $geo_cck_fieldname)) {
        $geo_cck[] = $field_detail;
        $geo_cck_fieldname[] = $field_detail['field_name'];
      }
    }
  }
  return $geo_cck;
}